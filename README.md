# Vehicle Ad Example

## Installation

In order to set up the app for local development un the following commands:
``` 
composer install
yarn
yarn run init
yarn run dev
```

The app should be completely installed at this point.
Run the following command to start the local web server:
``` 
php artisan serve
```

You should be able to access the app at http://localhost:8000 (or http://127.0.0.1:8000).


## License

[MIT license](https://opensource.org/licenses/MIT).
