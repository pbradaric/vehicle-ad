<?php

namespace App\Http\Controllers;

use App\Utils\AdResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AdController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {
        $adResource = new AdResource("https://www.classic-trader.com/api/vehicle-ad/");
        $adData = $adResource->getAd($id);
        $error = false;
        if (!$adData) {
            $error = true;
        }
        return view('ad', [
            'error' => $error,
            'adData' => $adData,
            'page' => 'ad',
        ]);
    }

}
