<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $carIds = [
            111688, 202738
        ];

        return view('home', [
            'carIds' => $carIds,
            'page' => 'home',
        ]);
    }
}
