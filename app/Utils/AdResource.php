<?php
namespace App\Utils;

use Illuminate\Support\Facades\Storage;

class AdResource
{
    private $resourceUrl = false;

    function __construct($resourceUrl)
    {
        $this->resourceUrl = rtrim($resourceUrl, '/') . '/';
    }

    public function getAd($id)
    {
        $adRawJson = $this->getRemoteRawJsonData($this->resourceUrl . $id);
        //Storage::disk('local')->put("ad{$id}.json", $adRawJson);
        $adArr = json_decode($adRawJson,TRUE);
        if (!is_array($adArr) || empty($adArr)) {
            return false;
        }
        if (!isset($adArr['success']) || !$adArr['success'] || !isset($adArr['data'])) {
            return false;
        }
        return $adArr['data'];
    }

    public function getRemoteRawJsonData($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
        ));
        $result = curl_exec($ch);
        curl_close($ch);
        return trim($result);
    }

}
