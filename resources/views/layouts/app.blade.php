<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <meta name="robots" content="noindex">
  <link rel="manifest" href="/manifest.json">
  <meta name="application-name" content="{{ Config::get('app.nicename') }}™">
  <title>{{ Config::get('app.nicename') }}</title>

  <link href="{{ mix('/css/app.css') }}" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template -->
  <link href="/css/shop-item.css" rel="stylesheet">

  <!-- Page specific styles -->
  @yield('styles')

</head>

<body>

  @include('partials.nav')

  @yield('content')

  @include('partials.footer')

  <script src="{{ mix('/js/app.js') }}"></script>

  <!-- Page specific scripts -->
  @yield('scripts')

</body>
</html>
