@section('content')
    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-lg-12">

                @if ($error)

                    <br>
                    <div class="alert alert-danger" role="alert">
                        There was some error while trying to load vehicle data. Try again later.
                    </div>
                    <br>

                @else

                    <div class="card mt-4">
                        <div id="carImages" class="carousel slide" data-ride="carousel" style="display: none;">
                            <ol class="carousel-indicators">
                                @foreach($adData['standardImages'] as $idx=>$image)
                                <li data-target="#carouselExampleIndicators" data-slide-to="{{ $idx }}" class="{{ $idx == 0 ? 'active' : '' }}"></li>
                                @endforeach
                            </ol>
                            <div class="carousel-inner">
                                @foreach($adData['standardImages'] as $idx=>$image)
                                <div class="carousel-item {{ $idx == 0 ? 'active' : '' }}">
                                    <img class="d-block w-100" src="{{ $image['url'] }}" alt="{{ $image['id'] }}">
                                </div>
                                @endforeach
                            </div>
                            <a class="carousel-control-prev" href="#carImages" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carImages" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                        <div class="card-body">
                            <h3 class="card-title">{{ $adData['car']['make'] }} {{ $adData['car']['model'] }} {{  $adData['yearOfProduction'] ? '('.$adData['yearOfProduction'].')' : '' }}</h3>
                            <h4></h4>
                            <p>Vehicle ID: {{ $adData['id'] }}</p>
                            @if (isset($adData['price']) && $adData['price'])
                                <p class="card-text">Price: <strong>{{ $adData['price'] }} {{ $adData['priceCurrency'] ?? '' }}</strong></p>
                            @endif
                        </div>
                    </div>

                    @if (isset($adData['contact']) && !empty($adData['contact']))
                    <div class="card card-outline-secondary my-4">
                        <div class="card-header">
                            Contact
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        @if (isset($adData['contact']['companyName']) && !empty($adData['contact']['companyName']))
                                            <tr><td><strong>{{ $adData['contact']['companyName'] }}</strong></td></tr>
                                        @endif
                                        @if (isset($adData['contact']['firstName']) || isset($adData['contact']['lastName']))
                                            @if (isset($adData['contact']['email']))
                                                <tr><td><a href="mailto:{{ $adData['contact']['email'] }}">{{ $adData['contact']['firstName'] ?? '' }} {{ $adData['contact']['lastName'] ?? '' }}</a></td></tr>
                                            @else
                                                <tr><td>{{ $adData['contact']['firstName'] ?? '' }} {{ $adData['contact']['lastName'] ?? '' }}</td></tr>
                                            @endif
                                        @endif
                                        @if (isset($adData['contact']['phone']) && !empty($adData['contact']['phone']))
                                            <tr><td>{{ $adData['contact']['phone'] }}</td></tr>
                                        @endif
                                        @if (isset($adData['contact']['email']) && !empty($adData['contact']['email']))
                                            <tr><td>{{ $adData['contact']['email'] }}</td></tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @endif

                    <ul class="nav nav-tabs" id="carDataTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="vehicle_data-tab" data-toggle="tab" href="#vehicle_data" role="tab" aria-controls="vehicle_data" aria-selected="true">Vehicle data</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="false">Description</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="condition-tab" data-toggle="tab" href="#condition" role="tab" aria-controls="condition" aria-selected="false">Condition</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="carDataTabContent">
                        <div class="tab-pane fade show active" id="vehicle_data" role="tabpanel" aria-labelledby="vehicle_data-tab">
                            @include('pages.ad.partial.vehicle_data')
                        </div>
                        <div class="tab-pane fade" id="description" role="tabpanel" aria-labelledby="description-tab">
                            @include('pages.ad.partial.description');
                        </div>
                        <div class="tab-pane fade" id="condition" role="tabpanel" aria-labelledby="condition-tab">
                            @include('pages.ad.partial.condition');
                        </div>
                    </div>
                @endif

            </div>
            <!-- /.col-lg-9 -->

        </div>

    </div>
    <!-- /.container -->
@endsection

@include('pages.ad.css.ad')

@include('pages.ad.js.ad')
