@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#carImages').show();
        $('#carImages').carousel();

        $('#carDataTab a').on('click', function (e) {
            e.preventDefault();
            $(this).tab('show');
        })

    });
</script>
@endsection
