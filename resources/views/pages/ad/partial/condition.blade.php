<div class="row">
    <div class="col-12">
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr><th colspan="2">Condition, Registrations & Documents</th></tr>
                </thead>
                <tbody>
                <tr><td>MOT until 11/2019</td><td></td></tr>
                <tr><td>Historic license plate</td><td>{{ $adData['oldtimerLicensePlate'] ? 'Yes' : 'No' }}</td></tr>
                <tr><td>Registered</td><td>{{ $adData['licensed'] ? 'Yes' : 'No' }}</td></tr>
                <tr><td>Ready to drive</td><td>{{ $adData['mainInspection'] ? 'Yes' : 'No' }}</td></tr>
                <tr><td>Documented restoration</td><td>{{ $adData['restorationImages'] ? 'Yes' : 'No' }}</td></tr>
                </tbody>
            </table>
        </div>
    </div>

</div>

