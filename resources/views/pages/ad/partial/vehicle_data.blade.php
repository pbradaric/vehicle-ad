<div class="row">
    <div class="col-md-6 col-xs-12">
        <div class="table-responsive">
            <table class="table">
                <tbody>
                <tr><td>Make</td><td>{{ $adData['car']['make'] ?? '' }}</td></tr>
                <tr><td>Model</td><td>{{ $adData['car']['model'] ?? '' }}</td></tr>
                <tr><td>Model name</td><td>{{ $adData['car']['model'] ?? '' }}</td></tr>
                <tr><td>Series</td><td>{{ $adData['specification'] ?? '' }}</td></tr>
                <tr><td>Year of manufacture</td><td>{{ $adData['yearOfProduction'] ?? '' }}</td></tr>
                <tr><td>Chassis number</td><td>{{ $adData['chassisNumber'] ?? '' }}</td></tr>
                <tr><td>Gearbox number</td><td>{{ $adData['gearboxNumber'] ?? '' }}</td></tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6 col-xs-12">
        <div class="table-responsive">
            <table class="table">
                <tbody>
                <tr><td>Report</td><td>{{ $adData['report'] ?? '' }}</td></tr>
                <tr><td>Condition category</td><td>{{ $adData['stateCategory'] ?? '' }}</td></tr>
                <tr><td>Mileage (read)</td><td>{{ $adData['mileageByUnit'] ?? '' }} {{ $adData['mileageUnit'] ?? '' }}</td></tr>
                <tr><td>Previous owners</td><td>{{ $adData['numberOfPreviousOwner'] ?? '' }}</td></tr>
                <tr><td>First registration</td><td>{{ $adData['firstRegistrationDate'] ?? '' }}</td></tr>
                <tr><td>Engine number</td><td>{{ $adData['engineNumber'] ?? '' }}</td></tr>
                <tr><td>Matching numbers</td><td>{{ $adData['matchingNumbers'] ?? '' }}</td></tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

