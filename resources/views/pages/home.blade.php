@section('content')
    <!-- Page Content -->
    <div class="container">

        <table class="table">
            <tbody>
            @foreach($carIds as $carId)
                <tr><td><a href="/ad/{{ $carId }}">Vehicle ID: {{ $carId }}</a></td></tr>
            @endforeach
            </tbody>
        </table>

    </div>
    <!-- /.container -->

@endsection

@section('styles')
<style>
</style>
@endsection

{{--@include('pages.home.js.home')--}}
